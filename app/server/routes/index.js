import Router from 'koa-router';
import apiRouter from './apiRouter';

const router = new Router();

router.use('/api', apiRouter.routes());
router.get("/", (ctx,next)=>{
    ctx.body= "Home Page";
})

module.exports = router;