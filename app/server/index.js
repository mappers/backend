import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import router from './routes';

const server = new Koa();

// server.use(async ctx => {
//     ctx.body = 'Hello World';
//   });
server.use(bodyParser());
server.use(router.routes());



server.listen(3000,console.log('server is working on PORT: http://localhost:3000/'));